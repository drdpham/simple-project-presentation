#!/usr/bin/env python3

# This scripts takes the content of a mermaid file and inserts it into a html template file.
# input arguments:
# -input: input mermaid file ; mandatory
# -config: configuration as a json file ; optional
#           example : {startOnLoad:true,gantt:{leftPadding:300}}
#           see https://github.com/mermaid-js/mermaid/issues/428 for json example and
#           see https://mermaid.js.org/config/schema-docs/config-defs-gantt-diagram-config.html for all options
#          default: {startOnLoad:true}

import argparse
import sys
import os

parser = argparse.ArgumentParser(description="Convert mermaid file to html")
parser.add_argument("-input", help="input mermaid file", required=True)
parser.add_argument("-config", help="configuration as a json file", required=False, type=str)
args = parser.parse_args()

# use first argument as mermaid file
mermaid_file = args.input
# Check that the file ends with .mmd
if not mermaid_file.endswith(".mmd"):
    print("Error: input file must be a .mmd file")
    sys.exit(1)

# Remove .mmd extension and add .html
html_file = mermaid_file[:-4] + ".html"

# Header of html file
html_header_1 = """<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <script src="https://cdn.jsdelivr.net/npm/mermaid/dist/mermaid.min.js"></script>
    <script>mermaid.initialize("""
html_header_2 = """);</script>
</head>
<body>
<pre class="mermaid">
"""

# if -config argument is not given, use default configuration
if args.config:
    html_header = html_header_1 + args.config + html_header_2
else:
    html_header = html_header_1 + "{startOnLoad:true}" + html_header_2


# Footer of html file
html_footer = """
</pre>
</body>
</html>
"""

# Read mermaid file
with open(mermaid_file, "r") as f:
    mermaid_content = f.read()

# Write html file
with open(html_file, "w") as f:
    f.write(html_header)
    f.write(mermaid_content)
    f.write(html_footer)


