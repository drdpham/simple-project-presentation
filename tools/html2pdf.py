#!/usr/bin/env python3

# This script uses Playwright to convert an html file to pdf
# Playwright is used because it renders the html file as a browser would do (in particular, it renders the javascript)

import os
import sys
import argparse

import asyncio
from playwright.async_api import async_playwright

async def run(playwright, html_file, output_pdf_file):
    browser = await playwright.chromium.launch()
    page = await browser.new_page()

    await page.goto(f"file:///{html_file}")
    await page.wait_for_timeout(2000)
    await page.pdf(path=output_pdf_file, format="A4")
    
    await page.close()
    await browser.close()

async def main(html_file, output_pdf_file):
    async with async_playwright() as playwright:
        await run(playwright, html_file, output_pdf_file)

# Use argparse to get arguments from command line
# -i or --input: input html file ; mandatory
# -d or --destination: output directory ; optional
# -o or --output: output pdf file ; optional

parser = argparse.ArgumentParser(description="Convert html file to pdf")
parser.add_argument("-input", help="input html file", required=True)
parser.add_argument("-destination", help="output directory")
parser.add_argument("-output", help="output pdf file")
args = parser.parse_args()

# use first argument as html file
html_file = args.input
# Check that the file ends with .html
if not html_file.endswith(".html"):
    print("Error: input file must be a .html file")
    sys.exit(1)

# Check if destination directory is specified
if args.destination:
    # Check that the directory exists
    if not os.path.isdir(args.destination):
        print("Error: destination directory does not exist")
        sys.exit(1)
    # Add a / at the end of the directory name if not present
    if not args.destination.endswith("/"):
        args.destination += "/"
else:
    # get path of input file
    html_file_path = os.path.dirname(html_file)
    # Add a / at the end of the directory name if not present
    if not html_file_path.endswith("/"):
        html_file_path += "/"
    args.destination = html_file_path

# Check if output file is specified
if args.output:
    # Check that the file ends with .pdf
    if not args.output.endswith(".pdf"):
        # Add .pdf extension to output file
        args.output += ".pdf"
    # Add the directory name to the output pdf file
    output_pdf_file = args.destination + args.output
else:
    # Get basename of html file
    html_file_basename = os.path.basename(html_file)
    # Remove .html extension and add .pdf
    output_pdf_file = html_file_basename[:-5] + ".pdf"
    # Add the directory name to the output pdf file
    output_pdf_file = args.destination + output_pdf_file


# Get full path to html file
html_file_full = os.path.abspath(html_file)

# Print paths
print(f"Input file: {html_file_full}")
print(f"Output file: {output_pdf_file}")

asyncio.run(main(html_file_full,output_pdf_file))
