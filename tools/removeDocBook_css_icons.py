#!/usr/bin/env python3

# This script parse a docbook file an searches for <span class="material-symbols-outlined">...</span> tags
# removes them. 

# the command takes the following arguments: 
# -input <input docbook file>
# -output <output docbook file> [optional]

# if no output file is specified, a new file with the same name as the input file is created with the suffix _nocssicons

import argparse
import sys
import os

parser = argparse.ArgumentParser(description="Remove unsupported tags from docbook file")
parser.add_argument("-input", help="input docbook file", required=True)
parser.add_argument("-output", help="output docbook file", required=False)
args = parser.parse_args()

# use first argument as docbook file
docbook_file = args.input
# Check that the file ends with .docbook
if not docbook_file.endswith(".docbook"):
    print("Error: input file must be a .docbook file")
    sys.exit(1)

# Read docbook file
with open(docbook_file, "r") as f:
    docbook_content = f.read()
    # Split the content on span tags
    docbook_blocks = docbook_content.split("span")
    # Loop over the blocks
    for i, block in enumerate(docbook_blocks):
        # If the block contains a material-symbols-outlined class
        if "material-symbols-outlined" in block:
            # Remove the block
            docbook_blocks[i] = block.replace(block, "")
    new_docbook_content = "delete".join(docbook_blocks)
    new_docbook_content = new_docbook_content.replace("<deletedelete>", "")
    # print(new_docbook_content)

# Check if the output file is specified
if args.output:
    # Write docbook file to the specified output file
    with open(args.output, "w") as f:
        f.write(new_docbook_content)
else: # Write docbook file to a new file with suffix _nocssicons
    docbook_file_sanitized = docbook_file[:-8] + "_nocssicons.docbook"
    with open(docbook_file_sanitized, "w") as f:
        f.write(new_docbook_content)

