#!/usr/bin/env python3

# This scripts takes a docbook file (xml) and removes several unsupported tags for docx conversion
# the unsupported tags are:
# <bibliodiv>...</bibliodiv>
# <bibliomixed>...</bibliomixed>
# These paired tags should just be removed, but everything inside it should be kept.
# Please note <bibliomisc>...</bibliomisc> is kept so that the xref tags are maintained functional.
# furthermore, it searches all tags "<anchor xml:id="labelid" xreflabel="reflabelstring"/>" and insert <xref linkend="labelid"/> just right after it.

import argparse
import sys
import os

parser = argparse.ArgumentParser(description="Remove unsupported tags from docbook file")
parser.add_argument("-input", help="input docbook file", required=True)
args = parser.parse_args()

# use first argument as docbook file
docbook_file = args.input
# Check that the file ends with .docbook
if not docbook_file.endswith(".docbook"):
    print("Error: input file must be a .docbook file")
    sys.exit(1)

# Read docbook file
with open(docbook_file, "r") as f:
    docbook_content = f.read()
    # search for the pair tag <bibliodiv>...</bibliodiv>
    # and remove it
    docbook_content = docbook_content.replace("<bibliodiv>", "")
    docbook_content = docbook_content.replace("</bibliodiv>", "")
    # search for the pair tag <bibliomixed>...</bibliomixed>
    # and remove it
    docbook_content = docbook_content.replace("<bibliomixed>", "")
    docbook_content = docbook_content.replace("</bibliomixed>", "")

# print(docbook_content)
# Now search for all tags "<anchor xml:id="labelid" xreflabel="[reflabelstring]"/>" and insert <xref linkend="labelid"/> just right after it.
# Use regex to find all tags
import re
# regex to find all tags
regex = r"<anchor xml:id=\".*?\" xreflabel=\".*?\"/>"
# find all tags
matches = re.finditer(regex, docbook_content, re.MULTILINE)
# loop over all matches
for matchNum, match in enumerate(matches, start=1):
    # get the tag
    tag = match.group()
    # get the labelid
    labelid = tag.split("xml:id=\"")[1].split("\"")[0]
    # print(labelid)
    # create the xref tag
    xref_tag = f"<xref linkend=\"{labelid}\"/>"
    # insert the xref tag just after the anchor tag
    docbook_content = docbook_content.replace(tag, tag + xref_tag)

# print(docbook_content)

# Write docbook file to a new file with suffix _sanitized4docx
docbook_file_sanitized = docbook_file[:-8] + "_sanitized4docx.docbook"
with open(docbook_file_sanitized, "w") as f:
    f.write(docbook_content)



    

    

    
    
