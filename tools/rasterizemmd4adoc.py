#!/usr/bin/env python3

# This script parse an adoc file an searches for mermaid code blocks defined as follows:
# <pre class="mermaid">
# include::data/mermaid/workplan.mmd[]
# </pre>
# And replaces this code block with:
# image::data/mermaid/workplan.png[]
# then it checks if the inserted image:: directive is inside a block defined by '++++'
# and if so, it moves the image:: directive outside of the block.

import argparse
import sys
import os

parser = argparse.ArgumentParser(description="Convert mermaid code blocks in adoc file to png images")
parser.add_argument("-input", help="input adoc file", required=True)
args = parser.parse_args()

# The adoc file to parse
adoc_file = args.input
# Check that the file ends with .adoc
if not adoc_file.endswith(".adoc"):
    print("Error: input file must be a .adoc file")
    sys.exit(1)

# parse the adoc file
with open(adoc_file, "r") as f:
    adoc_content = f.read()
    # Split the content of the adoc file into blocks defined by '++++'
    adoc_blocks = adoc_content.split("++++\n")
    # Loop over the blocks
    for i, block in enumerate(adoc_blocks):
        # If the block contains a mermaid code block
        if "<pre class=\"mermaid\">" in block:
            # Get the mermaid code block
            mermaid_block = block.split("<pre class=\"mermaid\">")[1].split("</pre>")[0]
            # Get the path of the mermaid file
            mermaid_file = mermaid_block.split("include::")[1].split("[]")[0]
            # Remove the .mmd extension and add .png
            png_file = mermaid_file[:-4] + ".png"
            # Replace the mermaid code block with the image:: directive
            adoc_blocks[i] = block.replace(block, f"image::{png_file}[]\n")
    new_adoc_content = "++++\n".join(adoc_blocks)
    # print(new_adoc_content)

# Check now if the image:: directive is inside a block defined as:
# ++++
# image::data/mermaid/workplan.png[]
# ++++
# and if so, replace the block with:
# image::data/mermaid/workplan.png[]
# Any other '++++' block is left untouched.

# Split the content of the adoc file into blocks defined by '++++'
adoc_blocks = new_adoc_content.split("++++\n")
adoc_blocks_new = []
skip = False
# Loop over the blocks
for i, block in enumerate(adoc_blocks):
    if skip:
        skip = False
        continue
    # If the block contains image::data/mermaid/ merge the last element of adoc_blocks_new with this block and also the next block
    if "image::data/mermaid/" in block:
        adoc_blocks_new[-1] = adoc_blocks_new[-1] + block + adoc_blocks[i+1]
        skip = True
    else:
        adoc_blocks_new.append(block)
new_adoc_content = "++++\n".join(adoc_blocks_new)
# print(new_adoc_content)

# Write the new adoc file to a file with suffix _temp_rasterdiag
adoc_file_temp = adoc_file[:-5] + "_temp_rasterdiag.adoc"
with open(adoc_file_temp, "w") as f:
    f.write(new_adoc_content)


