target_source ?= README.adoc
target_source_base := $(basename $(target_source))

target_pdf      := $(addprefix prod/pdf/,$(addsuffix .pdf,$(target_source_base)))
target_html     := $(addprefix prod/html/,$(addsuffix .html,$(target_source_base)))
target_html_art := $(addprefix prod/html/,$(addsuffix -article.html,$(target_source_base)))
target_docx     := $(addprefix prod/docx/,$(addsuffix .docx,$(target_source_base)))

targets := $(target_pdf) $(target_html) $(target_docx) $(target_html_art)

# print all targets
# $(info $$targets is [${targets}])

ODGS       = $(wildcard data/odgs/*.odg)
ODGS_SVG   = $(patsubst %.odg,%.svg,$(ODGS))
ODGS_PDF   = $(patsubst %.odg,%.pdf,$(ODGS))

DIAGs	  = $(wildcard data/mermaid/*.mmd)
DIAG_HTML = $(patsubst %.mmd,%.html,$(DIAGs))
DIAG_PDF  = $(patsubst %.mmd,%.pdf,$(DIAGs))
DIAG_PNG  = $(patsubst %.mmd,%.png,$(DIAGs))

NEEDED = $(ODGS_SVG) $(ODGS_PDF) $(DIAG_HTML) $(DIAG_PDF) $(DIAG_PNG)

# Get Inkscape version
INKSCAPEVERSION = $(shell inkscape --version | cut -f2 -d" ")
# inkscapever -> x.yy.z
INKSCAPEVERSION_MAJOR = $(shell echo $(INKSCAPEVERSION) | cut -f1 -d.)


all: $(targets)

$(targets) : dep

dep: $(NEEDED) 

%.pdf:%.odg
	libreoffice --headless --convert-to $(subst .,,$(suffix $@)) $< --outdir data/odgs
	pdfcrop --margins 1 $@ $@

%.svg:%.odg
	libreoffice --headless --convert-to $(subst .,,$(suffix $@)) $< --outdir data/odgs
	if [ "$(INKSCAPEVERSION_MAJOR)" = "0" ]; \
	then \
		inkscape --without-gui --export-area-drawing --file $@ --export-plain-svg $@; \
	else \
		inkscape --batch-process --actions "select-all;fit-canvas-to-selection;export-filename:$@;export-do;quit" $@ ; \
	fi

# out.o: src.c src.h
#   $@   # "out.o" (target)
#   $<   # "src.c" (first prerequisite)
#   $^   # "src.c src.h" (all prerequisites)

# %.o: %.c
#   $*   # the 'stem' with which an implicit rule matches ("foo" in "foo.c")

prod/html/%-article.html: %.adoc
	asciidoctor \
	-a stylesheet=data/css/asciidoc.css \
	-b html5 \
	-D $(dir $@) \
	-o $(notdir $@) $<

prod/html/%.html: %.adoc
	asciidoctor \
	-a stylesheet=data/css/asciidoc.css \
	-a toc=left \
	-a docinfo=shared \
	-a docinfodir=data/docinfo \
	-b html5 \
	-D $(dir $@) \
	-o $(notdir $@) $<

prod/pdf/%.pdf: prod/html/%-article.html
	tools/html2pdf.py -i $< -d prod/pdf/
	cd prod/pdf/ && mv $(basename $(notdir $<)).pdf $(notdir $@)

%.html: %.mmd
	tools/mermaid2html.py -i $< -c {startOnLoad:true,gantt:{leftPadding:400,sectionFontSize:16}}

%.pdf: %.html %.mmd
	tools/html2pdf.py -i $(basename $<).html -d $(dir $@)
	pdfcrop --margins 1 $@ $@

%.png: %.pdf %.mmd
	convert -density 300 $< $@

%_temp_rasterdiag.adoc: %.adoc
	# make a temporary adoc file in which mermaid diagrams are replaced by png images
	tools/rasterizemmd4adoc.py -i $<

%.docbook: %_temp_rasterdiag.adoc %.adoc
	# make a temporary docbook file in which biblio entries are sanitized
	asciidoctor \
	-d article \
	-b docbook \
	-D $(dir $@) \
	-o $*.docbook $<

%_sanitized4docx.docbook: %.docbook %_temp_rasterdiag.adoc %.adoc
	tools/removeDocBookbibliodiv.py -i $<
	tools/removeDocBook_css_icons.py -i $@ -o $@
	

prod/docx/%.docx: %_sanitized4docx.docbook
	pandoc -f docbook -t docx -o $@ $<

clean:
	rm -f $(NEEDED) *.docbook

cleanall: clean
	rm -f $(targets)

.INTERMEDIATE: %.docbook

# .PRECIOUS: %_temp_rasterdiag.adoc %_sanitized4docx.docbook %.docbook